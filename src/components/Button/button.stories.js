import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Button from '.';

export default { title: 'Button component', decorators: [withKnobs] };

export const buttonName = () => {
  const title = text('title', 'CLICK');
  return <Button title={title} />;
};
