import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { title } = props;
  return <button type="button">{title}</button>;
};

Button.propTypes = {
  title: PropTypes.string,
};

Button.defaultProps = {
  title: 'Click',
};

export default Button;
